'use strict';

describe('Service: reverseUrlService', function () {

  // load the service's module
    beforeEach(module('reverseApp'));
    //beforeEach(module('ngMock'));
    var reverseUrlService;
    var errors;
    beforeEach(inject(function (_reverseUrlService_, _$route_) {
        reverseUrlService = _reverseUrlService_;
        errors = [];
        reverseApp.value('$exceptionHandler', function (e, desc) {
            errors.push(e);
        });
    }));

  // instantiate service

  it('should find a simple url', function () {
      expect(reverseUrlService.reverse("home")).toBe("#/");
  });

  it('should return url without # if configured to do so', function () {
      expect(reverseUrlService.reverse("home", {}, { hashed: false })).toBe("/");
  });

  it('should find a parametrized url and map it correctly', function () {
      expect(reverseUrlService.reverse("user:edit", {id:42})).toBe("#/user/edit/42");
  });

  it('should find a parametrized url and map it correctly even in strict mode', function () {
      expect(reverseUrlService.reverse("user:edit", { id: 42 }, { strict: true })).toBe("#/user/edit/42");
  });

  it('should find a parametrized url and map it as far as possible when not in strict mode', function () {
      expect(reverseUrlService.reverse("user:edit:more", { id: 42 })).toBe("#/user/edit/42/:more");
  });

  it('should find a url using a different route property name', function () {
      expect(reverseUrlService.reverse("not:name", {}, {property: "prop"})).toBe("#/not/using/name");
  });

  it('should raise an exception if no matching url', inject(function ($exceptionHandler) {
      var url = reverseUrlService.reverse("something:thats:notthere", {});
      expect(errors[0]).toBe("NoMatchingUrl");
  }));

  it('should raise an exception if matching url but too much mapping values and strict mode', inject(function ($exceptionHandler) {
      var url = reverseUrlService.reverse("user:edit", { id: 42, toomuch: "Igor" }, { strict: true });
      expect(errors[0]).toBe("InvalidMapping");
  }));

  it('should raise an exception if matching url but too much mapping values and strict mode', inject(function ($exceptionHandler) {
      var url = reverseUrlService.reverse("user:edit", { id: 42, toomuch: "Igor" });
      expect(url).toBe("#/user/edit/42");
  }));

});
