'use strict';

describe('Service: fortytwoService', function () {

  // load the service's module
  beforeEach(module('reverseApp'));

  // instantiate service
  var fortytwoService;
  beforeEach(inject(function(_fortytwoService_) {
      fortytwoService = _fortytwoService_;
  }));

  it('should do something', function () {
      expect(fortytwoService.someMethod()).toBe(42);
  });

});
