'use strict';

describe('Service: fortytwo', function () {

  // load the service's module
  beforeEach(module('reverseApp'));

  // instantiate service
  var fortytwo;
  beforeEach(inject(function(_fortytwo_) {
      fortytwo = _fortytwo_;
  }));

  it('should do something', function () {
    expect(fortytwo).toBe(42);
  });

});
