'use strict';

describe('Directive: zestLink', function() {
  beforeEach(module('reverseApp'));

  var element;

  it('should replace zest-link with anchor tag, while keeping the text', inject(function($rootScope, $compile) {
      element = angular.element('<div><zest-link zest-name="user:edit" zest-mapping="{id:42}">this is the link content</zest-link></div>');
      element = $compile(element)($rootScope);
      var anchor = element.find("a"),
          zestLink = element.find("zest-link");
      expect(anchor.length).toBe(1);
      expect(zestLink.length).toBe(0);
      expect(anchor.text()).toBe('this is the link content');
  }));
});
