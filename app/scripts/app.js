'use strict';

var reverseApp = angular.module('reverseApp', [])
  .config(['$routeProvider', function($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'views/main.html',
        controller: 'MainCtrl',
        name: 'home'
      })
      .when('/user/edit/:id', {
        templateUrl: 'views/user/edit.html',
        controller: 'SomeCtrl',
        name: 'user:edit'
      })
      .when('/user/edit/:id/:more', {
        templateUrl: 'views/user/edit.html',
        controller: 'SomeCtrl',
        name: 'user:edit:more'
      })
      .when('/not/using/name', {
          templateUrl: 'views/user/edit.html',
          controller: 'SomeCtrl',
          prop: 'not:name'
      })
      .otherwise({
        redirectTo: '/'
      });
  }]);
