'use strict';

// Request for reverseUrlService from injector
reverseApp.directive('zestLink', function (reverseUrlService) {
    return {
        template: '<a ng-href="{{link}}" ng-transclude></a>', // Template to be used to create the new DOM element
        restrict: 'E', // Directive can only be called by using an element e.g. <zest-link ... > as opposed to <a zest-link=...
        replace: true, // Get rid of the element and replace it with the template
        transclude: true, // Keep whatever was inside the <zest-link> element and put it into the tag in our template which contains ng-transclude
        scope: {
            zestMapping: '&' // Evaluate the zest-mapping attribute into a function which will return the evaluated value.
        },
        // Link function is called on each instance of the directive separately
        link: function (scope, element, attrs) {
            var url = reverseUrlService.reverse(attrs.zestName, scope.zestMapping()); // Evaluate zest-mapping, and read zest-name, and ask the reverseUrlService for corresponding url
            scope.link = url; // Put the value into the template {{link}}
        }
    };
});